from Locators.WebLocators import locators


class Cryptos(locators):

    def __init__(self, driver):
        self.driver = driver

        self.CryptosButton = locators.CRYPTOS_BUTTON_BY_XPATH
        self.BitcoinArticle = locators.BITCOIN_ARTICLE_BUTTON_BY_XPATH
        self.EthereumArticle = locators.ETHEREUM_ARTICLE_BUTTON_BY_XPATH
        self.EthereumClassicArticle = locators.ETHEREUM_CLASSIC_ARTICLE_BUTTON_BY_XPATH
        self.DogeCoinArticle = locators.DOGECOIN_ARTICLE_BUTTON_BY_XPATH
        self.CardanoArticle = locators.CARDANO_ARTICLE_BUTTON_BY_XPATH
        self.EOSArticle = locators.EOS_ARTICLE_BUTTON_BY_XPATH
        self.RippleArticle = locators.RIPPLE_ARTICLE_BUTTON_BY_XPATH
        self.DashArticle = locators.DASH_ARTICLE_BUTTON_BY_XPATH
        self.MiotaArticle = locators.MIOTA_ARTICLE_BUTTON_BY_XPATH
        self.MoneroArticle = locators.MONERO_ARTICLE_BUTTON_BY_XPATH
        self.BitcoinHeading = locators.BITCOIN_MAIN_HEADING_BY_XPATH
        self.EthereumHeading = locators.ETHEREUM_MAIN_HEADING_BY_XPATH
        self.RippleHeading = locators.RIPPLE_MAIN_HEADING_BY_XPATH
        self.DashHeading = locators.DASH_MAIN_HEADING_BY_XPATH
        self.DogeCoinHeading = locators.DOGECOIN_MAIN_HEADING_BY_XPATH
        self.CardanoHeading = locators.CARDANO_MAIN_HEADING_BY_XPATH
        self.MiotaHeading = locators.MIOTA_MAIN_HEADING_BY_XPATH
        self.MoneroHeading = locators.MONERO_MAIN_HEADING_BY_XPATH
        self.EosHeading = locators.EOS_MAIN_HEADING_BY_XPATH
        self.EthereumClassicHeading = locators.ETHEREUM_CLASSIC_MAIN_HEADING_BY_XPATH

    def clickCryptosButton(self):
        self.driver.find_element_by_xpath(self.CryptosButton).click()

    def clickBitcoinArticle(self):
        self.driver.find_element_by_xpath(self.BitcoinArticle).click()

    def getBitcoinHeading(self):
        BitcoinHeading = self.driver.find_element_by_xpath(self.BitcoinHeading)
        return BitcoinHeading.text

    def clickEthereumArticle(self):
        self.driver.find_element_by_xpath(self.EthereumArticle).click()

    def getEthereumHeading(self):
        EthereumHeading = self.driver.find_element_by_xpath(self.EthereumHeading)
        return EthereumHeading.text

    def clickEthereumClassicArticle(self):
        self.driver.find_element_by_xpath(self.EthereumClassicArticle).click()

    def getEthereumClassicHeading(self):
        EthereumClassicHeading = self.driver.find_element_by_xpath(self.EthereumClassicHeading)
        return EthereumClassicHeading.text

    def clickDogeCoinArticle(self):
        self.driver.find_element_by_xpath(self.DogeCoinArticle).click()

    def getDogeCoinHeading(self):
        DogeCoinHeading = self.driver.find_element_by_xpath(self.DogeCoinHeading)
        return DogeCoinHeading.text

    def clickCardanoArticle(self):
        self.driver.find_element_by_xpath(self.CardanoArticle).click()

    def getCardanoHeading(self):
        CardanoHeading = self.driver.find_element_by_xpath(self.CardanoHeading)
        return CardanoHeading.text

    def clickEOSArticle(self):
        self.driver.find_element_by_xpath(self.EOSArticle).click()

    def getEosHeading(self):
        EosHeading = self.driver.find_element_by_xpath(self.EosHeading)
        return EosHeading.text

    def clickRippleArticle(self):
        self.driver.find_element_by_xpath(self.RippleArticle).click()

    def getRippleHeading(self):
        RippleHeading = self.driver.find_element_by_xpath(self.RippleHeading)
        return RippleHeading.text

    def clickDashArticle(self):
        self.driver.find_element_by_xpath(self.DashArticle).click()

    def getDashHeading(self):
        DashHeading = self.driver.find_element_by_xpath(self.DashHeading)
        return DashHeading.text

    def clickMiotaArticle(self):
        self.driver.find_element_by_xpath(self.MiotaArticle).click()

    def getMiotaHeading(self):
        MiotaHeading = self.driver.find_element_by_xpath(self.MiotaHeading)
        return MiotaHeading.text

    def clickMoneroArticle(self):
        self.driver.find_element_by_xpath(self.MoneroArticle).click()

    def getMoneroHeading(self):
        MoneroHeading = self.driver.find_element_by_xpath(self.MoneroHeading)
        return MoneroHeading.text
