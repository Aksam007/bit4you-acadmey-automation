from Locators.WebLocators import locators


class ExploreCryptos(locators):

    def __init__(self, driver):
        self.driver = driver

        self.BitcoinOnExploreCrypto = locators.BITCOIN_ON_EXPLORE_CRYPTO_BY_XPATH
        self.EthereumOnExploreCrypto = locators.ETHEREUM_ON_EXPLORE_CRYPTO_BY_XPATH
        self.RippleOnExploreCrypto = locators.RIPPLE_ON_EXPLORE_CRYPTO_BY_XPATH
        self.LiteCoinOnExploreCrypto = locators.LITECOIN_ON_EXPLORE_CRYPTO_BY_XPATH
        self.DogeCoinOnExploreCrypto = locators.DOGECOIN_ON_EXPLORE_CRYPTO_BY_XPATH
        self.ReadMoreOnExploreCrypto = locators.READ_MORE_ON_EXPLORE_CRYPTO_BY_XPATH

    def clickBitcoinOnExploreCrypto(self):
        self.driver.find_element_by_xpath(self.BitcoinOnExploreCrypto).click()

    def clickEthereumOnExploreCrypto(self):
        self.driver.find_element_by_xpath(self.EthereumOnExploreCrypto).click()

    def clickRippleOnExploreCrypto(self):
        self.driver.find_element_by_xpath(self.RippleOnExploreCrypto).click()

    def clickLiteCoinOnExploreCrypto(self):
        self.driver.find_element_by_xpath(self.LiteCoinOnExploreCrypto).click()

    def clickDogeCoinOnExploreCrypto(self):
        self.driver.find_element_by_xpath(self.DogeCoinOnExploreCrypto).click()

    def clickReadMoreOnExploreCrypto(self):
        self.driver.find_element_by_xpath(self.ReadMoreOnExploreCrypto).click()
