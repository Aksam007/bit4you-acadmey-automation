from Locators.WebLocators import locators


class Basics(locators):

    def __init__(self, driver):
        self.driver = driver

        self.BasicsButton = locators.BASICS_BUTTON_BY_XPATH
        self.DefinitionsButton = locators.DEFINITION_BUTTON_BY_XPATH
        self.ConceptsButton = locators.CONCEPT_BUTTON_BY_XPATH
        self.CryptographyButton = locators.CRYPTOGRAPHY_BUTTON_BY_XPATH
        self.NodesButton = locators.NODES_BUTTON_BY_XPATH
        self.CryptosUsedForButton = locators.CRYPTOS_USED_FOR_BUTTON_BY_XPATH
        self.BackButton = locators.BACK_BUTTON_BY_XPATH
        self.CryptocurrencyReadMore = locators.CRYPTOCURRENCY_READ_MORE_BUTTON_BY_XPATH
        self.MiningReadMore = locators.MINING_READ_MORE_BUTTON_BY_XPATH
        self.BlockchainReadMore = locators.BLOCKCHAIN_READ_MORE_BUTTON_BY_XPATH
        self.TradingReadMore = locators.TRADING_READ_MORE_BUTTON_BY_XPATH
        self.DefinitionsHeading = locators.DEFINITIONS_MAIN_HEADING_BY_XPATH
        self.ConceptHeading = locators.CONCEPT_MAIN_HEADING_BY_XPATH
        self.CryptographyHeading = locators.CRYPTOGRAPHY_MAIN_HEADING_BY_XPATH
        self.NodesHeading = locators.NODES_MAIN_HEADING_BY_XPATH
        self.CryptosUsedForHeading = locators.CRYPTOS_USED_FOR_MAIN_HEADING_BY_XPATH

    def clickBasicsButton(self):
        self.driver.find_element_by_xpath(self.BasicsButton).click()

    def clickDefinitionsButton(self):
        self.driver.find_element_by_xpath(self.DefinitionsButton).click()

    def getDefinitionsHeading(self):
        DefinitionsHeading = self.driver.find_element_by_xpath(self.DefinitionsHeading)
        return DefinitionsHeading.text

    def clickCryptocurrencyReadMoreButton(self):
        self.driver.find_element_by_xpath(self.CryptocurrencyReadMore).click()

    def clickMiningReadMore(self):
        self.driver.find_element_by_xpath(self.MiningReadMore).click()

    def clickBlockchainReadMore(self):
        self.driver.find_element_by_xpath(self.BlockchainReadMore).click()

    def clickTradingReadMore(self):
        self.driver.find_element_by_xpath(self.TradingReadMore).click()

    def clickConceptsButton(self):
        self.driver.find_element_by_xpath(self.ConceptsButton).click()

    def getConceptHeading(self):
        ConceptHeading = self.driver.find_element_by_xpath(self.ConceptHeading)
        return ConceptHeading.text

    def clickCryptographyButton(self):
        self.driver.find_element_by_xpath(self.CryptographyButton).click()

    def getCryptographyHeading(self):
        CryptographyMainHeading = self.driver.find_element_by_xpath(self.CryptographyHeading)
        return CryptographyMainHeading.text

    def clickNodesButton(self):
        self.driver.find_element_by_xpath(self.NodesButton).click()

    def getNodesHeading(self):
        NodeHeading = self.driver.find_element_by_xpath(self.NodesHeading)
        return NodeHeading.text

    def clickCryptosUsedForButton(self):
        self.driver.find_element_by_xpath(self.CryptosUsedForButton).click()

    def getCryptosUsedForHeading(self):
        CryptosUsedForHeading = self.driver.find_element_by_xpath(self.CryptosUsedForHeading)
        return CryptosUsedForHeading.text

    def clickBackButton(self):
        self.driver.find_element_by_xpath(self.BackButton).click()
