from Locators.WebLocators import locators


class BecomeAdvanceInCrypto(locators):

    def __init__(self, driver):
        self.driver = driver

        self.LearnButton = locators.LEARN_BUTTON_BY_XPATH
        self.TrainYourselfButton = locators.TRAIN_YOURSELF_BUTTON_BY_XPATH

    def clickLearnButton(self):
        self.driver.find_element_by_xpath(self.LearnButton).click()

    def clickTrainYourselfButton(self):
        self.driver.find_element_by_xpath(self.TrainYourselfButton).click()
        return self.driver.current_url
