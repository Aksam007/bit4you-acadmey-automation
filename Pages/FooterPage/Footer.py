from Locators.WebLocators import locators


class Footer(locators):

    def __init__(self, driver):
        self.driver = driver

        self.BasicInFooter = locators.BASICS_IN_FOOTER_BY_XPATH
        self.InvestmentInFooter = locators.INVESTMENT_IN_FOOTER_BY_XPATH
        self.TransactionInFooter = locators.TRANSACTION_IN_FOOTER_BY_XPATH
        self.CryptosInFooter = locators.CRYPTOS_IN_FOOTER_BY_XPATH
        self.EvolutionInFooter = locators.EVOLUTION_IN_FOOTER_BY_XPATH
        self.ProductionInFooter = locators.PRODUCTION_IN_FOOTER_BY_XPATH
        self.VisitWebsiteInFooter = locators.VISIT_WEBSITE_IN_FOOTER_BY_XPATH
        self.ContactUsInFooter = locators.CONTACT_US_IN_FOOTER_BY_XPATH
        self.FacebookButton = locators.FACEBOOK_BUTTON_BY_XPATH
        self.InstagramButton = locators.INSTAGRAM_BUTTON_BY_XPATH
        self.TwitterButton = locators.TWITTER_BUTTON_BY_XPATH
        self.GitlabButton = locators.GITLAB_BUTTON_BY_XPATH

    def clickBasicInFooter(self):
        self.driver.find_element_by_xpath(self.BasicInFooter).click()

    def clickInvestmentInFooter(self):
        self.driver.find_element_by_xpath(self.InvestmentInFooter).click()

    def clickTransactionInFooter(self):
        self.driver.find_element_by_xpath(self.TransactionInFooter).click()

    def clickCryptosInFooter(self):
        self.driver.find_element_by_xpath(self.CryptosInFooter).click()

    def clickEvolutionInFooter(self):
        self.driver.find_element_by_xpath(self.EvolutionInFooter).click()

    def clickProductionInFooter(self):
        self.driver.find_element_by_xpath(self.ProductionInFooter).click()

    def clickVisitWebsiteInFooter(self):
        self.driver.find_element_by_xpath(self.VisitWebsiteInFooter).click()

    def clickContactUsInFooter(self):
        self.driver.find_element_by_xpath(self.ContactUsInFooter).click()

    def clickFacebookButtonFooter(self):
        self.driver.find_element_by_xpath(self.FacebookButton).click()

    def clickInstagramInFooter(self):
        self.driver.find_element_by_xpath(self.InstagramButton).click()

    def clickTwitterInFooter(self):
        self.driver.find_element_by_xpath(self.TwitterButton).click()

    def clickGitlabInFooter(self):
        self.driver.find_element_by_xpath(self.GitlabButton).click()
