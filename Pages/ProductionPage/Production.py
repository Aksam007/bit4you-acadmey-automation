from Locators.WebLocators import locators


class Production(locators):

    def __init__(self, driver):
        self.driver = driver

        self.ProductionButton = locators.PRODUCTION_BUTTON_BY_XPATH
        self.CanIMineByMyselfButton = locators.CAN_I_MINE_BY_MYSELF_BUTTON_BY_XPATH
        self.CanIMineByMyselfHeading = locators.CAN_I_MINE_BY_MYSELF_HEADING_BY_XPATH
        self.MiningButton = locators.MINING_BUTTON_BY_XPATH
        self.MiningHeading = locators.MINING_HEADING_BY_XPATH
        self.BlockchainButton = locators.BLOCKCHAIN_BUTTON_BY_XPATH
        self.BlockchainHeading = locators.BLOCKCHAIN_HEADING_BY_XPATH

    def clickProductionButton(self):
        self.driver.find_element_by_xpath(self.ProductionButton).click()

    def clickCanIMineByMyselfButton(self):
        self.driver.find_element_by_xpath(self.CanIMineByMyselfButton).click()

    def getCanIMineByMyselfHeading(self):
        CanIMineByMyself = self.driver.find_element_by_xpath(self.CanIMineByMyselfHeading)
        return CanIMineByMyself.text

    def clickMiningButton(self):
        self.driver.find_element_by_xpath(self.MiningButton).click()

    def getMiningHeading(self):
        MiningHeading = self.driver.find_element_by_xpath(self.MiningHeading)
        return MiningHeading.text

    def clickBlockchainButton(self):
        self.driver.find_element_by_xpath(self.BlockchainButton).click()

    def getBlockchainHeading(self):
        Blockchain = self.driver.find_element_by_xpath(self.BlockchainHeading)
        return Blockchain.text
