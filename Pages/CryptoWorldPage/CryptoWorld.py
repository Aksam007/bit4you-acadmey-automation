from Locators.WebLocators import locators


class CryptoWorld(locators):

    def __init__(self, driver):
        self.driver = driver

        self.RSIOnCryptoWorld = locators.RSI_ON_CRYPTO_WORLD_BY_XPATH
        self.FibanocciOnCryptoWorld = locators.FIBONACCI_ON_CRYPTO_WORLD_BY_XPATH
        self.ICHIMOKUOnCryptoWorld = locators.ICHIMOKU_ON_CRYPTO_WORLD_BY_XPATH
        self.ElliotWaveCryptoWorld = locators.ELLIOTT_WAVE_ON_CRYPTO_WORLD_BY_XPATH
        self.ReadMoreOnCryptoWorld = locators.READ_MORE_BUTTON_ON_CRYPTO_WORLD_BY_XPATH

    def clickRSIOnCryptoWorldButton(self):
        self.driver.find_element_by_xpath(self.RSIOnCryptoWorld).click()

    def clickFibanocciOnCryptoWorldButton(self):
        self.driver.find_element_by_xpath(self.FibanocciOnCryptoWorld).click()

    def clickICHIMOKUOnCryptoWorldButton(self):
        self.driver.find_element_by_xpath(self.ICHIMOKUOnCryptoWorld).click()

    def clickElliotWaveCryptoWorldButton(self):
        self.driver.find_element_by_xpath(self.ElliotWaveCryptoWorld).click()

    def clickReadMoreOnCryptoWorldButton(self):
        self.driver.find_element_by_xpath(self.ReadMoreOnCryptoWorld).click()
