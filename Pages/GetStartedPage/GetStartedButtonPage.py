from Locators.WebLocators import locators


class GetStarted(locators):

    def __init__(self, driver):
        self.driver = driver

        self.GetStartedButton = locators.GET_STARTED_BUTTON_BY_XPATH

    def clickGetStartedButton(self):
        self.driver.find_element_by_xpath(self.GetStartedButton).click()
