from Locators.WebLocators import locators


class ContactUs(locators):

    def __init__(self, driver):
        self.driver = driver

        self.ContactUsButton = locators.CONTACT_US_BUTTON_BY_XPATH

    def clickContactUsButton(self):
        self.driver.find_element_by_xpath(self.ContactUsButton).click()
