from Locators.WebLocators import locators


class LearnAboutProduction(locators):

    def __init__(self, driver):
        self.driver = driver

        self.BlockchainButton = locators.BLOCK_CHAIN_BUTTON_BY_XPATH
        self.MiningButton = locators.MINING_ON_LEARN_ABOUT_PRODUCTION_BUTTON_BY_XPATH
        self.ReadMoreButton = locators.READ_MORE_ON_LEARN_ABOUT_PRODUCTION_BUTTON_BY_XPATH

    def clickBlockchainButton(self):
        self.driver.find_element_by_xpath(self.BlockchainButton).click()

    def clickMiningButton(self):
        self.driver.find_element_by_xpath(self.MiningButton).click()

    def clickReadMoreButton(self):
        self.driver.find_element_by_xpath(self.ReadMoreButton).click()
