from Locators.WebLocators import locators


class BuyOrSellCrypto(locators):

    def __init__(self, driver):
        self.driver = driver

        self.BuyingButton = locators.BUYING_BUTTON_BY_XPATH
        self.SellingButton = locators.SELLING_BUTTON_BY_XPATH
        self.StopLossButton = locators.STOP_LOSS_BY_XPATH
        self.TakeProfitButton = locators.TAKE_PROFIT_BY_XPATH
        self.ExchangeButton = locators.EXCHANGE_BUTTON_BY_XPATH
        self.ReadMoreButtonOnBuyAndSellCrypto = locators.READ_MORE_IN_BUY_AND_SELL_BY_XPATH

    def clickBuyingButton(self):
        self.driver.find_element_by_xpath(self.BuyingButton).click()

    def clickSellingButton(self):
        self.driver.find_element_by_xpath(self.SellingButton).click()

    def clickStopLossButton(self):
        self.driver.find_element_by_xpath(self.StopLossButton).click()

    def clickTakeProfitButton(self):
        self.driver.find_element_by_xpath(self.TakeProfitButton).click()

    def clickExchangeButton(self):
        self.driver.find_element_by_xpath(self.ExchangeButton).click()

    def clickReadMoreButtonOnBuyAndSellCrypto(self):
        self.driver.find_element_by_xpath(self.ReadMoreButtonOnBuyAndSellCrypto).click()
