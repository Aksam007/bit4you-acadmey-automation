from Locators.WebLocators import locators


class Evolution(locators):

    def __init__(self, driver):
        self.driver = driver

        self.EvolutionButton = locators.EVOLUTION_BUTTON_BY_XPATH
        self.HistoryOfCryptoButton = locators.HISTORY_OF_CRYPTO_BUTTON_BY_XPATH
        self.HistoryOfCryptoHeading = locators.HISTORY_OF_CRYPTO_HEADING_BY_XPATH
        self.WallStreetBetsButton = locators.WALLSTREETBETS_BUTTON_BY_XPATH
        self.WallStreetBetsHeading = locators.WALLSTREETBETS_HEADING_BY_XPATH
        self.BitcoinIsBecomingPopularButton = locators.BITCOIN_BECOMING_POPULAR_BUTTON_BY_XPATH
        self.BitcoinIsBecomingPopularHeading = locators.BITCOIN_BECOMING_POPULAR_HEADING_BY_XPATH

    def clickEvolutionButton(self):
        self.driver.find_element_by_xpath(self.EvolutionButton).click()

    def clickHistoryOfCryptoButton(self):
        self.driver.find_element_by_xpath(self.HistoryOfCryptoButton).click()

    def getHistoryOfCryptoHeading(self):
        HistoryOfCrypto = self.driver.find_element_by_xpath(self.HistoryOfCryptoHeading)
        return HistoryOfCrypto.text

    def clickWallStreetBetsButton(self):
        self.driver.find_element_by_xpath(self.WallStreetBetsButton).click()

    def getWallStreetBetsHeading(self):
        WallStreetBets = self.driver.find_element_by_xpath(self.WallStreetBetsHeading)
        return WallStreetBets.text

    def clickBitcoinIsBecomingPopularButton(self):
        self.driver.find_element_by_xpath(self.BitcoinIsBecomingPopularButton).click()

    def getBitcoinIsBecomingPopularHeading(self):
        BitcoinIsBecomingPopular = self.driver.find_element_by_xpath(self.BitcoinIsBecomingPopularHeading)
        return BitcoinIsBecomingPopular.text
