from Locators.WebLocators import locators


class Investment(locators):

    def __init__(self, driver):
        self.driver = driver

        self.InvestmentButton = locators.INVESTMENT_BUTTON_BY_XPATH
        self.RsiReadMoreButton = locators.RSI_READ_MORE_BY_XPATH
        self.DowTheoryReadMoreButton = locators.DOW_THEORY_READ_MORE_BY_XPATH
        self.IchimokuReadMoreButton = locators.ICHIMOKU_READ_MORE_BY_XPATH
        self.PayingWithCryptoReadMoreButton = locators.PAYING_WITH_CRYPTO_READ_MORE_BY_XPATH
        self.MovingAveragesReadMoreButton = locators.MOVING_AVERAGES_READ_MORE_BY_XPATH
        self.TechnicalAnalysisReadMoreButton = locators.TECHNICAL_ANALYSIS_READ_MORE_BY_XPATH
        self.BollingerBrandsReadMoreButton = locators.BOLLINGER_BRANDS_READ_MORE_BY_XPATH
        self.SavingsInBitcoinsReadMoreButton = locators.SAVING_IN_BITCOINS_READ_MORE_BY_XPATH
        self.RsiMainHeading = locators.RSI_MAIN_HEADING_BY_XPATH
        self.DowTheoryMainHeading = locators.DOW_THEORY_MAIN_HEADING_BY_XPATH
        self.ICHIMOKUMainHeading = locators.ICHIMOKU_MAIN_HEADING_BY_XPATH
        self.PayingWithCryptoMainHeading = locators.PAYING_WITH_CRYPTO_MAIN_HEADING_BY_XPATH
        self.MovingAveragesMainHeading = locators.MOVING_AVERAGES_MAIN_HEADING_BY_XPATH
        self.TechnicalAnalysisMainHeading = locators.TECHNICAL_ANALYSIS_MAIN_HEADING_BY_XPATH
        self.BollingerBrandsMainHeading = locators.BOLLINGER_BRANDS_MAIN_HEADING_BY_XPATH
        self.SavingInBitcoinsMainHeading = locators.SAVING_IN_BITCOINS_MAIN_HEADING_BY_XPATH

    def clickInvestmentButton(self):
        self.driver.find_element_by_xpath(self.InvestmentButton).click()

    def clickRsiReadMoreButton(self):
        self.driver.find_element_by_xpath(self.RsiReadMoreButton).click()

    def getRSIMainHeading(self):
        RSIHeading = self.driver.find_element_by_xpath(self.RsiMainHeading)
        return RSIHeading.text

    def clickDowTheoryReadMoreButton(self):
        self.driver.find_element_by_xpath(self.DowTheoryReadMoreButton).click()

    def getDowTheoryMainHeading(self):
        DowTheoryHeading = self.driver.find_element_by_xpath(self.DowTheoryMainHeading)
        return DowTheoryHeading.text

    def clickIchimokuReadMoreButton(self):
        self.driver.find_element_by_xpath(self.IchimokuReadMoreButton).click()

    def getICHIMOKUMainHeading(self):
        ICHIMOKUHeading = self.driver.find_element_by_xpath(self.ICHIMOKUMainHeading)
        return ICHIMOKUHeading.text

    def clickPayingWithCryptoReadMoreButton(self):
        self.driver.find_element_by_xpath(self.PayingWithCryptoReadMoreButton).click()

    def getPayingWithCryptoMainHeading(self):
        PayingWithCryptoHeading = self.driver.find_element_by_xpath(self.PayingWithCryptoMainHeading)
        return PayingWithCryptoHeading.text

    def clickMovingAveragesReadMoreButton(self):
        self.driver.find_element_by_xpath(self.MovingAveragesReadMoreButton).click()

    def getMovingAveragesMainHeading(self):
        MovingAveragesHeading = self.driver.find_element_by_xpath(self.MovingAveragesMainHeading)
        return MovingAveragesHeading.text

    def clickTechnicalAnalysisReadMoreButton(self):
        self.driver.find_element_by_xpath(self.TechnicalAnalysisReadMoreButton).click()

    def getTechnicalAnalysisMainHeading(self):
        TechnicalAnalysisHeading = self.driver.find_element_by_xpath(self.TechnicalAnalysisMainHeading)
        return TechnicalAnalysisHeading.text

    def clickBollingerBrandsReadMoreButton(self):
        self.driver.find_element_by_xpath(self.BollingerBrandsReadMoreButton).click()

    def getBollingerBrandsMainHeading(self):
        BollingerBrandsHeading = self.driver.find_element_by_xpath(self.BollingerBrandsMainHeading)
        return BollingerBrandsHeading.text

    def clickSavingsInBitcoinsReadMoreButton(self):
        self.driver.find_element_by_xpath(self.SavingsInBitcoinsReadMoreButton).click()

    def getSavingInBitcoinsMainHeading(self):
        SavingInBitcoinsHeading = self.driver.find_element_by_xpath(self.SavingInBitcoinsMainHeading)
        return SavingInBitcoinsHeading.text
