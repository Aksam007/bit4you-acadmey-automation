from selenium.webdriver import ActionChains

from Locators.WebLocators import locators


class ChangeLanguage(locators):

    def __init__(self, driver):
        self.driver = driver

        self.ChangeLanguageButton = locators.CHANGE_LANGUAGE_BUTTON_BY_XPATH
        self.FrancaisButton = locators.FRANCAIS_BUTTON_BY_XPATH
        self.NederlandsButton = locators.NEDERLANDS_BUTTON_BY_XPATH
        self.DeutschButton = locators.DEUTSCH_BUTTON_BY_XPATH
        self.EspanolButton = locators.ESPANOL_BUTTON_BY_XPATH

    def clickFrancaisButton(self):
        actions = ActionChains(self.driver)
        actions.move_to_element(self.driver.find_element_by_xpath(self.ChangeLanguageButton))
        actions.perform()
        self.driver.find_element_by_xpath(self.FrancaisButton).click()

    def clickNederlandsButton(self):
        actions = ActionChains(self.driver)
        actions.move_to_element(self.driver.find_element_by_xpath(self.ChangeLanguageButton))
        actions.perform()
        self.driver.find_element_by_xpath(self.NederlandsButton).click()

    def clickDeutschButton(self):
        actions = ActionChains(self.driver)
        actions.move_to_element(self.driver.find_element_by_xpath(self.ChangeLanguageButton))
        actions.perform()
        self.driver.find_element_by_xpath(self.DeutschButton).click()

    def clickEspanolButton(self):
        actions = ActionChains(self.driver)
        actions.move_to_element(self.driver.find_element_by_xpath(self.ChangeLanguageButton))
        actions.perform()
        self.driver.find_element_by_xpath(self.EspanolButton).click()
