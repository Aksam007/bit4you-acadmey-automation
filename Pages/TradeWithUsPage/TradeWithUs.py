from Locators.WebLocators import locators


class TradeWithUs(locators):

    def __init__(self, driver):
        self.driver = driver

        self.TradeWithUsButton = locators.TRADE_WITH_US_BUTTON_BY_XPATH

    def clickTradeWithUsButton(self):
        Bit4youWebsiteButton = self.driver.find_element_by_xpath(self.TradeWithUsButton)
        Bit4youWebsiteButton.click()


