from Locators.WebLocators import locators


class Transaction(locators):

    def __init__(self, driver):
        self.driver = driver

        self.TransactionButton = locators.TRANSACTION_BUTTON_BY_XPATH
        self.BuyCrypto = locators.BUY_CRYPTO_BUTTON_BY_XPATH
        self.SellCrypto = locators.SELL_CRYPTO_BUTTON_BY_XPATH
        self.ExchangeCrypto = locators.EXCHANGE_CRYPTO_BUTTON_BY_XPATH
        self.StopLoss = locators.STOP_LOSS_BUTTON_BY_XPATH
        self.TakeProfit = locators.TAKE_PROFIT_BUTTON_BY_XPATH
        self.HowBlockchainWorks = locators.HOW_BLOCKCHAIN_WORKS_BY_XPATH
        self.InfoThroughNodes = locators.INFO_THROUGH_NODES_BY_XPATH
        self.AreBlockchainSecure = locators.ARE_BLOCKCHAIN_SECURE_BY_XPATH
        self.BuyCryptoHeading = locators.BUY_CRYPTO_MAIN_HEADING_BY_XPATH
        self.SellCryptoHeading = locators.SELL_CRYPTO_MAIN_HEADING_BY_XPATH
        self.ExchangeCryptoHeading = locators.EXCHANGE_CRYPTO_MAIN_HEADING_BY_XPATH
        self.StopLossHeading = locators.STOP_LOSS_CRYPTO_MAIN_HEADING_BY_XPATH
        self.TakeProfitHeading = locators.TAKE_PROFIT_MAIN_HEADING_BY_XPATH
        self.HowBlockchainWorksHeading = locators.HOW_BLOCKCHAIN_WORKS_MAIN_HEADING_BY_XPATH
        self.InfoThroughNodesHeading = locators.INFO_THROUGH_NODES_MAIN_HEADING_BY_XPATH
        self.AreBlockchainSecureHeading = locators.ARE_BLOCKCHAIN_SECURE_HEADING_BY_XPATH

    def clickTransactionButton(self):
        self.driver.find_element_by_xpath(self.TransactionButton).click()

    def clickBuyCryptoButton(self):
        self.driver.find_element_by_xpath(self.BuyCrypto).click()

    def getBuyCryptoHeading(self):
        BuyCryptoHeading = self.driver.find_element_by_xpath(self.BuyCryptoHeading)
        return BuyCryptoHeading.text

    def clickSellCryptoButton(self):
        self.driver.find_element_by_xpath(self.SellCrypto).click()

    def getSellCryptoHeading(self):
        SellCryptoHeading = self.driver.find_element_by_xpath(self.SellCryptoHeading)
        return SellCryptoHeading.text

    def clickExchangeCryptoButton(self):
        self.driver.find_element_by_xpath(self.ExchangeCrypto).click()

    def getExchangeCryptoHeading(self):
        ExchangeCryptoHeading = self.driver.find_element_by_xpath(self.ExchangeCryptoHeading)
        return ExchangeCryptoHeading.text

    def clickStopLossButton(self):
        self.driver.find_element_by_xpath(self.StopLoss).click()

    def getStopLossHeading(self):
        StopLossHeading = self.driver.find_element_by_xpath(self.StopLossHeading)
        return StopLossHeading.text

    def clickTakeProfitButton(self):
        self.driver.find_element_by_xpath(self.TakeProfit).click()

    def getTakeProfitHeading(self):
        TakeProfitHeading = self.driver.find_element_by_xpath(self.TakeProfitHeading)
        return TakeProfitHeading.text

    def clickHowBlockchainWorksButton(self):
        self.driver.find_element_by_xpath(self.HowBlockchainWorks).click()

    def getHowBlockchainWorksHeading(self):
        HowBlockchainWorksHeading = self.driver.find_element_by_xpath(self.HowBlockchainWorksHeading)
        return HowBlockchainWorksHeading.text

    def clickInfoThroughNodesButton(self):
        self.driver.find_element_by_xpath(self.InfoThroughNodes).click()

    def getInfoThroughNodesHeading(self):
        InfoThroughNodesHeading = self.driver.find_element_by_xpath(self.InfoThroughNodesHeading)
        return InfoThroughNodesHeading.text

    def clickAreBlockchainSecureButton(self):
        self.driver.find_element_by_xpath(self.AreBlockchainSecure).click()

    def getAreBlockchainSecureHeading(self):
        AreBlockchainSecureHeading = self.driver.find_element_by_xpath(self.AreBlockchainSecureHeading)
        return AreBlockchainSecureHeading.text
