from Locators.WebLocators import locators


class CryptoCommunity(locators):

    def __init__(self, driver):
        self.driver = driver

        self.History = locators.HISTORY_OF_THE_CRYPTO_XPATH
        self.WallStreetbet = locators.WALLSTREETBET_BUTTON_BY_XPATH
        self.ElonMusk = locators.ELON_MUSK_BUTTON_BY_XPATH
        self.ReadMoreButtonOnCryptoCommunity = locators.READ_MORE_BUTTON_ON_CRYPTO_COMMUNITY_BY_XPATH

    def clickHistoryButtonOnCryptoCommunity(self):
        self.driver.find_element_by_xpath(self.History).click()

    def clickWallStreetbetOnCryptoCommunity(self):
        self.driver.find_element_by_xpath(self.WallStreetbet).click()

    def clickElonMuskOnCryptoCommunity(self):
        self.driver.find_element_by_xpath(self.ElonMusk).click()

    def clickReadMoreButtonOnCryptoCommunity(self):
        self.driver.find_element_by_xpath(self.ReadMoreButtonOnCryptoCommunity).click()
