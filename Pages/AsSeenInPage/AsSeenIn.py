from Locators.WebLocators import locators


class AsSeenIn(locators):

    def __init__(self, driver):
        self.driver = driver

        self.LESOIRButton = locators.LE_SOIR_BUTTON_BY_XPATH
        self.SUR7Button = locators.SUR_7_BUTTON_BY_XPATH
        self.LALIBREButton = locators.LA_LIBRE_BUTTON_BY_XPATH
        self.CanalZButton = locators.CANAL_z_BUTTON_BY_XPATH
        self.LN24Button = locators.LN_24_BUTTON_BY_XPATH
        self.TrendsButton = locators.TRENDS_BUTTON_BY_XPATH

    def clickLESOIRButton(self):
        self.driver.find_element_by_xpath(self.LESOIRButton).click()

    def clickSUR7Button(self):
        self.driver.find_element_by_xpath(self.SUR7Button).click()

    def clickLALIBREButton(self):
        self.driver.find_element_by_xpath(self.LALIBREButton).click()

    def clickCanalZButton(self):
        self.driver.find_element_by_xpath(self.CanalZButton).click()

    def clickLN24Button(self):
        self.driver.find_element_by_xpath(self.LN24Button).click()

    def clickTrendsButton(self):
        self.driver.find_element_by_xpath(self.TrendsButton).click()
