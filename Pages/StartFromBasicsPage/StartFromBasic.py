from Locators.WebLocators import locators


class StartFromBasics(locators):

    def __init__(self, driver):
        self.driver = driver

        self.DefinitionsButton = locators.DEFINITIONS_BUTTON_BY_XPATH
        self.ConceptsButton = locators.CONCEPTS_BUTTON_BY_XPATH
        self.CryptoGraphysButton = locators.CRYPTOGRAPHYS_BUTTON_BY_XPATH
        self.ReadMoreButton = locators.READ_MORE_BUTTON_BY_XPATH

    def clickDefinitionsButton(self):
        self.driver.find_element_by_xpath(self.DefinitionsButton).click()

    def clickConceptsButton(self):
        self.driver.find_element_by_xpath(self.ConceptsButton).click()

    def clickCryptoGraphysButton(self):
        self.driver.find_element_by_xpath(self.CryptoGraphysButton).click()

    def clickReadMoreButton(self):
        self.driver.find_element_by_xpath(self.ReadMoreButton).click()
