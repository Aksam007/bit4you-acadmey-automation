from Pages.AsSeenInPage.AsSeenIn import AsSeenIn
from TestCases.TestRunnerConfig import TestRunner


class doAsSeenInPageTest(TestRunner):

    def test_LESOIR(self):
        driver = self.driver

        AsSeenInObj = AsSeenIn(driver)
        AsSeenInObj.clickLESOIRButton()

    def test_SUR7(self):
        driver = self.driver

        AsSeenInObj = AsSeenIn(driver)
        AsSeenInObj.clickSUR7Button()

    def test_CanalZ(self):
        driver = self.driver

        AsSeenInObj = AsSeenIn(driver)
        AsSeenInObj.clickCanalZButton()

    def test_LALIBRE(self):
        driver = self.driver

        AsSeenInObj = AsSeenIn(driver)
        AsSeenInObj.clickLALIBREButton()

    def test_LN24(self):
        driver = self.driver

        AsSeenInObj = AsSeenIn(driver)
        AsSeenInObj.clickLN24Button()

    def test_Trends(self):
        driver = self.driver

        AsSeenInObj = AsSeenIn(driver)
        AsSeenInObj.clickTrendsButton()
