import time

from Pages.EvolutionPage.Evolution import Evolution
from TestCases.TestRunnerConfig import TestRunner
from TestData.TestData import TestData


class doEvolutionPageTest(TestRunner):

    def test_EvolutionButton(self):
        driver = self.driver

        EvolutionObj = Evolution(driver)
        EvolutionObj.clickEvolutionButton()

    def test_HistoryOfCrypto(self):
        driver = self.driver

        EvolutionObj = Evolution(driver)
        EvolutionObj.clickEvolutionButton()
        time.sleep(5)
        EvolutionObj.clickHistoryOfCryptoButton()
        HistoryOfCrypto = EvolutionObj.getHistoryOfCryptoHeading()
        assert HistoryOfCrypto == TestData.HistoryOfCryptosHeading

    def test_WallStreetBets(self):
        driver = self.driver

        EvolutionObj = Evolution(driver)
        EvolutionObj.clickEvolutionButton()
        time.sleep(5)
        EvolutionObj.clickWallStreetBetsButton()
        WallStreetBetsHeading = EvolutionObj.getWallStreetBetsHeading()
        assert WallStreetBetsHeading == TestData.WallStreetBetsHeading

    def test_BitcoinBecomingPopular(self):
        driver = self.driver

        EvolutionObj = Evolution(driver)
        EvolutionObj.clickEvolutionButton()
        time.sleep(5)
        EvolutionObj.clickBitcoinIsBecomingPopularButton()
        BitcoinIsBecomingPopularHeading = EvolutionObj.getBitcoinIsBecomingPopularHeading()
