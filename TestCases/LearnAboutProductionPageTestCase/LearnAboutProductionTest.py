from Pages.LearnAboutProductionPage.LearnAboutProduction import LearnAboutProduction
from TestCases.TestRunnerConfig import TestRunner


class doLearnAboutProductionPageTest(TestRunner):

    def test_Blockchain_On_Learn_About_Production(self):
        driver = self.driver

        LearnAboutProductionObj = LearnAboutProduction(driver)
        LearnAboutProductionObj.clickBlockchainButton()

    def test_Mining_On_Learn_About_Production(self):
        driver = self.driver

        LearnAboutProductionObj = LearnAboutProduction(driver)
        LearnAboutProductionObj.clickMiningButton()

    def test_ReadMore_On_Learn_About_Production(self):
        driver = self.driver

        LearnAboutProductionObj = LearnAboutProduction(driver)
        LearnAboutProductionObj.clickReadMoreButton()
