from Pages.StartFromBasicsPage.StartFromBasic import StartFromBasics
from TestCases.TestRunnerConfig import TestRunner


class doStartFromBasicsPageTest(TestRunner):

    def test_Definitions(self):
        driver = self.driver

        StartFromBasicsObj = StartFromBasics(driver)
        StartFromBasicsObj.clickDefinitionsButton()

    def test_Concepts(self):
        driver = self.driver

        StartFromBasicsObj = StartFromBasics(driver)
        StartFromBasicsObj.clickConceptsButton()

    def test_CryptoGraphys(self):
        driver = self.driver

        StartFromBasicsObj = StartFromBasics(driver)
        StartFromBasicsObj.clickCryptoGraphysButton()

    def test_ReadMore(self):
        driver = self.driver

        StartFromBasicsObj = StartFromBasics(driver)
        StartFromBasicsObj.clickReadMoreButton()
