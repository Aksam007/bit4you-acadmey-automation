import time

from Pages.ProductionPage.Production import Production
from TestCases.TestRunnerConfig import TestRunner
from TestData.TestData import TestData


class doProductionPageTest(TestRunner):

    def test_Production(self):
        driver = self.driver

        ProductionObj = Production(driver)
        ProductionObj.clickProductionButton()

    def test_CanIMineByMyself(self):
        driver = self.driver

        ProductionObj = Production(driver)
        ProductionObj.clickProductionButton()
        time.sleep(10)
        ProductionObj.clickCanIMineByMyselfButton()
        time.sleep(10)
        CanIMineByMyselfHeading = ProductionObj.getCanIMineByMyselfHeading()
        assert CanIMineByMyselfHeading == TestData.CanIMineByMyselfHeading

    def test_Mining(self):
        driver = self.driver

        ProductionObj = Production(driver)
        ProductionObj.clickProductionButton()
        time.sleep(10)
        ProductionObj.clickMiningButton()
        time.sleep(10)
        MiningHeading = ProductionObj.getMiningHeading()
        assert MiningHeading == TestData.MiningHeading

    def test_Blockchain(self):
        driver = self.driver

        ProductionObj = Production(driver)
        ProductionObj.clickProductionButton()
        time.sleep(10)
        ProductionObj.clickBlockchainButton()
        time.sleep(10)
        BlockchainHeading = ProductionObj.getBlockchainHeading()
        assert BlockchainHeading == TestData.BlockchainHeading
