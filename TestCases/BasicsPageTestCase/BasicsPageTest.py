import time

from Pages.BasicsPage.Basics import Basics
from TestData.TestData import TestData
from TestCases.TestRunnerConfig import TestRunner


class doBasicsPageTest(TestRunner):

    def test_Basics(self):
        driver = self.driver

        BasicsObj = Basics(driver)
        BasicsObj.clickBasicsButton()

    def test_DefinitionsArticle(self):
        driver = self.driver

        BasicsObj = Basics(driver)
        BasicsObj.clickBasicsButton()
        BasicsObj.clickDefinitionsButton()
        DefinitionHeading = BasicsObj.getDefinitionsHeading()
        assert DefinitionHeading == TestData.DefinitionsHeading

    def test_DefinitionsArticle_Mining(self):
        driver = self.driver

        BasicsObj = Basics(driver)
        BasicsObj.clickBasicsButton()
        BasicsObj.clickDefinitionsButton()
        BasicsObj.clickMiningReadMore()

    def test_DefinitionsArticle_Blockchain(self):
        driver = self.driver

        BasicsObj = Basics(driver)
        BasicsObj.clickBasicsButton()
        BasicsObj.clickDefinitionsButton()
        BasicsObj.clickBlockchainReadMore()

    def test_DefinitionsArticle_Trading(self):
        driver = self.driver

        BasicsObj = Basics(driver)
        BasicsObj.clickBasicsButton()
        BasicsObj.clickDefinitionsButton()
        BasicsObj.clickTradingReadMore()

    def test_ConceptArticle(self):
        driver = self.driver

        BasicsObj = Basics(driver)
        BasicsObj.clickBasicsButton()
        BasicsObj.clickConceptsButton()
        ConceptHeading = BasicsObj.getConceptHeading()
        assert ConceptHeading == TestData.ConceptHeading

    def test_CryptographyArticle(self):
        driver = self.driver

        BasicsObj = Basics(driver)
        BasicsObj.clickBasicsButton()
        BasicsObj.clickCryptographyButton()
        CryptographyMainHeading = BasicsObj.getCryptographyHeading()
        assert CryptographyMainHeading == TestData.CryptographyHeading

    def test_NodesArticle(self):
        driver = self.driver

        BasicsObj = Basics(driver)
        BasicsObj.clickBasicsButton()
        BasicsObj.clickNodesButton()
        NodesHeading = BasicsObj.getNodesHeading()
        assert NodesHeading == TestData.NodesHeading

    def test_CryptoUsedForArticle(self):
        driver = self.driver

        BasicsObj = Basics(driver)
        BasicsObj.clickBasicsButton()
        BasicsObj.clickCryptosUsedForButton()
        CryptosUsedForHeading = BasicsObj.getCryptosUsedForHeading()
        assert CryptosUsedForHeading == TestData.CryptosUsedForHeading

    def test_BackButton(self):
        driver = self.driver

        BasicsObj = Basics(driver)
        BasicsObj.clickBasicsButton()
        BasicsObj.clickDefinitionsButton()
        BasicsObj.clickBackButton()
