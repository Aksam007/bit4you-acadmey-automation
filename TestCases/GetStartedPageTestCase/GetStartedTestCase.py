from Pages.GetStartedPage.GetStartedButtonPage import GetStarted
from TestCases.TestRunnerConfig import TestRunner


class doGetStartedPageTest(TestRunner):

    def test_GetStarted(self):
        driver = self.driver

        GetStartedObj = GetStarted(driver)
        GetStartedObj.clickGetStartedButton()
