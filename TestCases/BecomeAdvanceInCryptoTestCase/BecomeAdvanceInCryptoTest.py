from Pages.BecomeAdvanceInCryptoPage.BecomeAdvanceInCrypto import BecomeAdvanceInCrypto
from TestCases.TestRunnerConfig import TestRunner
from TestData.TestData import TestData


class doBecomeAdvanceInCryptoPageTest(TestRunner):

    def test_ContactUs(self):
        driver = self.driver

        BecomeAdvanceInCryptoObj = BecomeAdvanceInCrypto(driver)
        BecomeAdvanceInCryptoObj.clickLearnButton()

    def test_TrainYourself(self):
        driver = self.driver

        BecomeAdvanceInCryptoObj = BecomeAdvanceInCrypto(driver)
        Bit4youWebsite = BecomeAdvanceInCryptoObj.clickTrainYourselfButton()
        assert Bit4youWebsite == TestData.Bit4youWebsiteLink
