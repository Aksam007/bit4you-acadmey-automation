from Pages.CryptoCommunityPage.CryptoCommunityPage import CryptoCommunity
from TestCases.TestRunnerConfig import TestRunner


class doCryptoCommunityPageTest(TestRunner):

    def test_HistoryButtonOnCryptoCommunity(self):
        driver = self.driver

        CryptoCommunityObj = CryptoCommunity(driver)
        CryptoCommunityObj.clickHistoryButtonOnCryptoCommunity()

    def test_WallStreetbetOnCryptoCommunity(self):
        driver = self.driver

        CryptoCommunityObj = CryptoCommunity(driver)
        CryptoCommunityObj.clickWallStreetbetOnCryptoCommunity()

    def test_ElonMuskOnCryptoCommunity(self):
        driver = self.driver

        CryptoCommunityObj = CryptoCommunity(driver)
        CryptoCommunityObj.clickElonMuskOnCryptoCommunity()

    def test_ReadMoreButtonOnCryptoCommunity(self):
        driver = self.driver

        CryptoCommunityObj = CryptoCommunity(driver)
        CryptoCommunityObj.clickReadMoreButtonOnCryptoCommunity()
