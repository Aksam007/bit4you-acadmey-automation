from Pages.FooterPage.Footer import Footer
from TestCases.TestRunnerConfig import TestRunner


class doFooterPageTest(TestRunner):

    def test_BasicInFooter(self):
        driver = self.driver

        FooterObj = Footer(driver)
        FooterObj.clickBasicInFooter()

    def test_CryptosInFooter(self):
        driver = self.driver

        FooterObj = Footer(driver)
        FooterObj.clickCryptosInFooter()

    def test_EvolutionInFooter(self):
        driver = self.driver

        FooterObj = Footer(driver)
        FooterObj.clickEvolutionInFooter()

    def test_InvestmentInFooter(self):
        driver = self.driver

        FooterObj = Footer(driver)
        FooterObj.clickInvestmentInFooter()

    def test_ProductionInFooter(self):
        driver = self.driver

        FooterObj = Footer(driver)
        FooterObj.clickProductionInFooter()

    def test_TransactionInFooter(self):
        driver = self.driver

        FooterObj = Footer(driver)
        FooterObj.clickTransactionInFooter()

    def test_VisitWebsiteInFooter(self):
        driver = self.driver

        FooterObj = Footer(driver)
        FooterObj.clickVisitWebsiteInFooter()

    def test_ContactUsInFooter(self):
        driver = self.driver

        FooterObj = Footer(driver)
        FooterObj.clickContactUsInFooter()

    def test_FacebookInFooter(self):
        driver = self.driver

        FooterObj = Footer(driver)
        FooterObj.clickFacebookButtonFooter()

    def test_InstagramInFooter(self):
        driver = self.driver

        FooterObj = Footer(driver)
        FooterObj.clickInstagramInFooter()

    def test_TwitterInFooter(self):
        driver = self.driver

        FooterObj = Footer(driver)
        FooterObj.clickTwitterInFooter()

    def test_GitlabInFooter(self):
        driver = self.driver

        FooterObj = Footer(driver)
        FooterObj.clickGitlabInFooter()


