from Pages.ExploreCryptosPage.ExploreCryptos import ExploreCryptos
from TestCases.TestRunnerConfig import TestRunner


class doExploreCryptosPageTest(TestRunner):

    def test_BitcoinOnExploreCrypto(self):
        driver = self.driver

        ExploreCryptosObj = ExploreCryptos(driver)
        ExploreCryptosObj.clickBitcoinOnExploreCrypto()

    def test_EthereumOnExploreCrypto(self):
        driver = self.driver

        ExploreCryptosObj = ExploreCryptos(driver)
        ExploreCryptosObj.clickEthereumOnExploreCrypto()

    def test_RippleOnExploreCrypto(self):
        driver = self.driver

        ExploreCryptosObj = ExploreCryptos(driver)
        ExploreCryptosObj.clickRippleOnExploreCrypto()

    def test_LiteCoinOnExploreCrypto(self):
        driver = self.driver

        ExploreCryptosObj = ExploreCryptos(driver)
        ExploreCryptosObj.clickLiteCoinOnExploreCrypto()

    def test_DogeCoinOnExploreCrypto(self):
        driver = self.driver

        ExploreCryptosObj = ExploreCryptos(driver)
        ExploreCryptosObj.clickDogeCoinOnExploreCrypto()

    def test_ReadMoreOnExploreCrypto(self):
        driver = self.driver

        ExploreCryptosObj = ExploreCryptos(driver)
        ExploreCryptosObj.clickReadMoreOnExploreCrypto()
