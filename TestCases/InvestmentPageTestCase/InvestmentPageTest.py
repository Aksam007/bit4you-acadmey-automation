import time

from Pages.InvestmentPage.Investment import Investment
from TestCases.TestRunnerConfig import TestRunner
from TestData.TestData import TestData


class doInvestmentPageTest(TestRunner):

    def test_InvestmentButton(self):
        driver = self.driver

        InvestmentObj = Investment(driver)
        InvestmentObj.clickInvestmentButton()

    def test_RSIButton(self):
        driver = self.driver

        InvestmentObj = Investment(driver)
        InvestmentObj.clickInvestmentButton()
        time.sleep(5)
        InvestmentObj.clickRsiReadMoreButton()
        RSI = InvestmentObj.getRSIMainHeading()
        assert RSI == TestData.RSIMainHeading

    def test_DowTheoryButton(self):
        driver = self.driver

        InvestmentObj = Investment(driver)
        InvestmentObj.clickInvestmentButton()
        time.sleep(5)
        InvestmentObj.clickDowTheoryReadMoreButton()
        DowTheory = InvestmentObj.getDowTheoryMainHeading()
        assert DowTheory == TestData.DowTheoryMainHeading

    def test_ICHIMOKUButton(self):
        driver = self.driver

        InvestmentObj = Investment(driver)
        InvestmentObj.clickInvestmentButton()
        time.sleep(5)
        InvestmentObj.clickIchimokuReadMoreButton()
        ICHIMOKU = InvestmentObj.getICHIMOKUMainHeading()
        assert ICHIMOKU == TestData.ICHIMOKUMainHeading

    def test_PayingWithCryptoButton(self):
        driver = self.driver

        InvestmentObj = Investment(driver)
        InvestmentObj.clickInvestmentButton()
        time.sleep(5)
        InvestmentObj.clickPayingWithCryptoReadMoreButton()
        PayingWithCrypto = InvestmentObj.getPayingWithCryptoMainHeading()
        assert PayingWithCrypto == TestData.PayingWithCryptoMainHeading

    def test_MovingAveragesButton(self):
        driver = self.driver

        InvestmentObj = Investment(driver)
        InvestmentObj.clickInvestmentButton()
        time.sleep(5)
        InvestmentObj.clickMovingAveragesReadMoreButton()
        MovingAverages = InvestmentObj.getMovingAveragesMainHeading()
        assert MovingAverages == TestData.MovingAveragesMainHeading

    def test_TechnicalAnalysisButton(self):
        driver = self.driver

        InvestmentObj = Investment(driver)
        InvestmentObj.clickInvestmentButton()
        time.sleep(3)
        InvestmentObj.clickTechnicalAnalysisReadMoreButton()
        TechnicalAnalysis = InvestmentObj.getTechnicalAnalysisMainHeading()
        assert TechnicalAnalysis == TestData.TechnicalAnalysisMainHeading

    def test_BollingerBrandsButton(self):
        driver = self.driver

        InvestmentObj = Investment(driver)
        InvestmentObj.clickInvestmentButton()
        time.sleep(5)
        InvestmentObj.clickBollingerBrandsReadMoreButton()
        BollingerBrands = InvestmentObj.getBollingerBrandsMainHeading()
        assert BollingerBrands == TestData.BollingerBrandsMainHeading

    def test_SavingsInCryptoButton(self):
        driver = self.driver

        InvestmentObj = Investment(driver)
        InvestmentObj.clickInvestmentButton()
        time.sleep(5)
        InvestmentObj.clickSavingsInBitcoinsReadMoreButton()
        SavingInBitcoins = InvestmentObj.getSavingInBitcoinsMainHeading()
        assert SavingInBitcoins == TestData.SavingsInBitcoinMainHeading
