from Pages.BuyOrSellCryptoPage.BuyOrSellCrypto import BuyOrSellCrypto
from TestCases.TestRunnerConfig import TestRunner


class doBuyOrSellCryptoPageTest(TestRunner):

    def test_Selling(self):
        driver = self.driver

        BuyOrSellCryptoObj = BuyOrSellCrypto(driver)
        BuyOrSellCryptoObj.clickSellingButton()

    def test_Buying(self):
        driver = self.driver

        BuyOrSellCryptoObj = BuyOrSellCrypto(driver)
        BuyOrSellCryptoObj.clickBuyingButton()

    def test_StopLoss(self):
        driver = self.driver

        BuyOrSellCryptoObj = BuyOrSellCrypto(driver)
        BuyOrSellCryptoObj.clickStopLossButton()

    def test_TakeProfit(self):
        driver = self.driver

        BuyOrSellCryptoObj = BuyOrSellCrypto(driver)
        BuyOrSellCryptoObj.clickTakeProfitButton()

    def test_Exchange(self):
        driver = self.driver

        BuyOrSellCryptoObj = BuyOrSellCrypto(driver)
        BuyOrSellCryptoObj.clickExchangeButton()

    def test_ReadMoreButtonOnBuyAndSellCrypto(self):
        driver = self.driver

        BuyOrSellCryptoObj = BuyOrSellCrypto(driver)
        BuyOrSellCryptoObj.clickReadMoreButtonOnBuyAndSellCrypto()
