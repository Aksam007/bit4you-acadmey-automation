from Pages.CryptosPage.Cryptos import Cryptos
from TestCases.TestRunnerConfig import TestRunner
from TestData.TestData import TestData


class doCryptoPageTest(TestRunner):

    def test_CryptoButton(self):
        driver = self.driver

        CryptosObj = Cryptos(driver)
        CryptosObj.clickCryptosButton()

    def test_BitcoinButton(self):
        driver = self.driver

        CryptosObj = Cryptos(driver)
        CryptosObj.clickCryptosButton()
        CryptosObj.clickBitcoinArticle()
        BitcoinHeading = CryptosObj.getBitcoinHeading()
        assert BitcoinHeading == TestData.BitcoinHeading

    def test_EthereumButton(self):
        driver = self.driver

        CryptosObj = Cryptos(driver)
        CryptosObj.clickCryptosButton()
        CryptosObj.clickEthereumArticle()
        EthereumHeading = CryptosObj.getEthereumHeading()
        assert EthereumHeading == TestData.EthereumHeading

    def test_EthereumClassicButton(self):
        driver = self.driver

        CryptosObj = Cryptos(driver)
        CryptosObj.clickCryptosButton()
        CryptosObj.clickEthereumClassicArticle()
        EthereumClassicHeading = CryptosObj.getEthereumClassicHeading()
        assert EthereumClassicHeading == TestData.EthereumClassicHeading

    def test_EOSButton(self):
        driver = self.driver

        CryptosObj = Cryptos(driver)
        CryptosObj.clickCryptosButton()
        CryptosObj.clickEOSArticle()
        EosHeading = CryptosObj.getEosHeading()
        assert EosHeading == TestData.EosHeading

    def test_CardanoButton(self):
        driver = self.driver

        CryptosObj = Cryptos(driver)
        CryptosObj.clickCryptosButton()
        CryptosObj.clickCardanoArticle()
        CardanoHeading = CryptosObj.getCardanoHeading()
        assert CardanoHeading == TestData.CardanoHeading

    def test_RippleButton(self):
        driver = self.driver

        CryptosObj = Cryptos(driver)
        CryptosObj.clickCryptosButton()
        CryptosObj.clickRippleArticle()
        RippleHeading = CryptosObj.getRippleHeading()
        assert RippleHeading == TestData.RippleHeading

    def test_DogeCoinButton(self):
        driver = self.driver

        CryptosObj = Cryptos(driver)
        CryptosObj.clickCryptosButton()
        CryptosObj.clickDogeCoinArticle()
        DogeCoinHeading = CryptosObj.getDogeCoinHeading()
        assert DogeCoinHeading == TestData.DogeCoinHeading

    def test_DashButton(self):
        driver = self.driver

        CryptosObj = Cryptos(driver)
        CryptosObj.clickCryptosButton()
        CryptosObj.clickDashArticle()
        DashHeading = CryptosObj.getDashHeading()
        assert DashHeading == TestData.DashHeading

    def test_MiotaButton(self):
        driver = self.driver

        CryptosObj = Cryptos(driver)
        CryptosObj.clickCryptosButton()
        CryptosObj.clickMiotaArticle()
        MiotaHeading = CryptosObj.getMiotaHeading()
        assert MiotaHeading == TestData.MiotaHeading

    def test_MoneroButton(self):
        driver = self.driver

        CryptosObj = Cryptos(driver)
        CryptosObj.clickCryptosButton()
        CryptosObj.clickMoneroArticle()
        MoneroHeading = CryptosObj.getMoneroHeading()
        assert MoneroHeading == TestData.MoneroHeading

