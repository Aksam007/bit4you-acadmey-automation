import unittest
import os
from HTMLTestRunner import HTMLTestRunner

from ContactUsTestCase.ContactUs import doContactUsPageTest
from BasicsPageTestCase.BasicsPageTest import doBasicsPageTest
from InvestmentPageTestCase.InvestmentPageTest import doInvestmentPageTest
from TransactionPageTestCase.TransactionPageTest import doTransactionPageTest
from ProductionPageTestCase.ProductionTestCase import doProductionPageTest
from EvolutionPageTestCase.EvolutionTestCase import doEvolutionPageTest
from CryptosPageTestCase.CryptosTestCase import doCryptoPageTest
from TradeWithUsPageTestCase.TradeWithUsTestCase import doTradeWithUsPageTest
from BuyAndSellCryptoPageTestCase.BuyAndSellCryptoTest import doBuyOrSellCryptoPageTest
from CryptoWorldPageTestCase.CryptoWorldPageTest import doCryptoWorldPageTest
from StartFromBasicsPageTestCase.StartFromBasicsPageTestCase import doStartFromBasicsPageTest
from ExploreCryptoPageTestCase.ExploreCryptoPageTest import doExploreCryptosPageTest
from CryptoCommunityPageTestCase.CryptoCommunityPageTest import doCryptoCommunityPageTest
from FooterTestCase.FooterTest import doFooterPageTest
from GetStartedPageTestCase.GetStartedTestCase import doGetStartedPageTest
from LearnAboutProductionPageTestCase.LearnAboutProductionTest import doLearnAboutProductionPageTest
from ChangeLanguageTestCase.ChangeLanguageTest import doChangeLanguage
from AsSeenInPageTestCase.AsSeenInPageTest import doAsSeenInPageTest
from BecomeAdvanceInCryptoTestCase.BecomeAdvanceInCryptoTest import doBecomeAdvanceInCryptoPageTest

if __name__ == "__main__":
    ContactUsPage_tests = unittest.TestLoader().loadTestsFromTestCase(doContactUsPageTest)
    BasicsPage_tests = unittest.TestLoader().loadTestsFromTestCase(doBasicsPageTest)
    InvestmentPage_tests = unittest.TestLoader().loadTestsFromTestCase(doInvestmentPageTest)
    TransactionPage_tests = unittest.TestLoader().loadTestsFromTestCase(doTransactionPageTest)
    ProductionPage_tests = unittest.TestLoader().loadTestsFromTestCase(doProductionPageTest)
    EvolutionPage_tests = unittest.TestLoader().loadTestsFromTestCase(doEvolutionPageTest)
    CryptoPage_tests = unittest.TestLoader().loadTestsFromTestCase(doCryptoPageTest)
    TradeWithUs_tests = unittest.TestLoader().loadTestsFromTestCase(doTradeWithUsPageTest)
    BuyOrSellCrypto_tests = unittest.TestLoader().loadTestsFromTestCase(doBuyOrSellCryptoPageTest)
    CryptoWorld_tests = unittest.TestLoader().loadTestsFromTestCase(doCryptoWorldPageTest)
    ExploreCryptos_tests = unittest.TestLoader().loadTestsFromTestCase(doExploreCryptosPageTest)
    StartFromBasics_tests = unittest.TestLoader().loadTestsFromTestCase(doStartFromBasicsPageTest)
    CryptoCommunity_tests = unittest.TestLoader().loadTestsFromTestCase(doCryptoCommunityPageTest)
    Footer_tests = unittest.TestLoader().loadTestsFromTestCase(doFooterPageTest)
    GetStarted_tests = unittest.TestLoader().loadTestsFromTestCase(doGetStartedPageTest)
    LearnAboutProduction_tests = unittest.TestLoader().loadTestsFromTestCase(doLearnAboutProductionPageTest)
    ChangeLanguage_tests = unittest.TestLoader().loadTestsFromTestCase(doChangeLanguage)
    AsSeenIn_tests = unittest.TestLoader().loadTestsFromTestCase(doAsSeenInPageTest)
    BecomeAdvanceInCrypto_tests = unittest.TestLoader().loadTestsFromTestCase(doBecomeAdvanceInCryptoPageTest)

    test_suite = unittest.TestSuite([ContactUsPage_tests, BasicsPage_tests, InvestmentPage_tests,
                                     TransactionPage_tests, ProductionPage_tests, EvolutionPage_tests,
                                     CryptoPage_tests, TradeWithUs_tests, BuyOrSellCrypto_tests,
                                     CryptoWorld_tests, StartFromBasics_tests, ExploreCryptos_tests,
                                     CryptoCommunity_tests, Footer_tests, GetStarted_tests, LearnAboutProduction_tests,
                                     ChangeLanguage_tests, AsSeenIn_tests, BecomeAdvanceInCrypto_tests])
    currentDir = os.getcwd()
    outfile = open(currentDir + "\Bit4youAutomationReport.html", "wb")
    runner = HTMLTestRunner(
        stream=outfile,
        title='Bit4you Automation Report',
        description='Bit4you Automation Tests'
    )
    runner.run(test_suite)
