from Pages.ChangeWebsiteLanguage.ChangeWebLanguage import ChangeLanguage
from TestCases.TestRunnerConfig import TestRunner


class doChangeLanguage(TestRunner):

    def test_FrancaisLanguage(self):
        driver = self.driver

        ChangeLanguageObj = ChangeLanguage(driver)
        ChangeLanguageObj.clickFrancaisButton()

    def test_DeutschLanguage(self):
        driver = self.driver

        ChangeLanguageObj = ChangeLanguage(driver)
        ChangeLanguageObj.clickDeutschButton()

    def test_EspanolButton(self):
        driver = self.driver

        ChangeLanguageObj = ChangeLanguage(driver)
        ChangeLanguageObj.clickEspanolButton()

    def test_NederlandsButton(self):
        driver = self.driver

        ChangeLanguageObj = ChangeLanguage(driver)
        ChangeLanguageObj.clickNederlandsButton()
