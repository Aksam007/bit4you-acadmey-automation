import time

from Pages.TransactionPage.Transaction import Transaction
from TestCases.TestRunnerConfig import TestRunner
from TestData.TestData import TestData


class doTransactionPageTest(TestRunner):

    def test_TransactionButton(self):
        driver = self.driver

        TransactionObj = Transaction(driver)
        TransactionObj.clickTransactionButton()

    def test_BuyCrypto(self):
        driver = self.driver

        TransactionObj = Transaction(driver)
        TransactionObj.clickTransactionButton()
        time.sleep(5)
        TransactionObj.clickBuyCryptoButton()
        BuyCryptoHeading = TransactionObj.getBuyCryptoHeading()
        assert BuyCryptoHeading == TestData.BuyCryptoHeading

    def test_SellCrypto(self):
        driver = self.driver

        TransactionObj = Transaction(driver)
        TransactionObj.clickTransactionButton()
        time.sleep(5)
        TransactionObj.clickSellCryptoButton()
        SellCryptoHeading = TransactionObj.getSellCryptoHeading()
        assert SellCryptoHeading == TestData.SellCryptoHeading

    def test_ExchangeCrypto(self):
        driver = self.driver

        TransactionObj = Transaction(driver)
        TransactionObj.clickTransactionButton()
        time.sleep(5)
        TransactionObj.clickExchangeCryptoButton()
        ExchangeCryptoHeading = TransactionObj.getExchangeCryptoHeading()
        assert ExchangeCryptoHeading == TestData.ExchangeCryptoHeading

    def test_StopLoss(self):
        driver = self.driver

        TransactionObj = Transaction(driver)
        TransactionObj.clickTransactionButton()
        time.sleep(5)
        TransactionObj.clickStopLossButton()
        StopLossHeading = TransactionObj.getStopLossHeading()
        assert StopLossHeading == TestData.StopLossHeading

    def test_TakeProfit(self):
        driver = self.driver

        TransactionObj = Transaction(driver)
        TransactionObj.clickTransactionButton()
        time.sleep(5)
        TransactionObj.clickTakeProfitButton()
        TakeProfitHeading = TransactionObj.getTakeProfitHeading()
        assert TakeProfitHeading == TestData.TakeProfitHeading

    def test_HowBlockchainWorks(self):
        driver = self.driver

        TransactionObj = Transaction(driver)
        TransactionObj.clickTransactionButton()
        time.sleep(5)
        TransactionObj.clickHowBlockchainWorksButton()
        HowBlockchainWorksHeading = TransactionObj.getHowBlockchainWorksHeading()
        assert HowBlockchainWorksHeading == TestData.HowBlockchainWorksHeading

    def test_InfoThroughNodes(self):
        driver = self.driver

        TransactionObj = Transaction(driver)
        TransactionObj.clickTransactionButton()
        time.sleep(5)
        TransactionObj.clickInfoThroughNodesButton()
        InfoThroughNodesHeading = TransactionObj.getInfoThroughNodesHeading()
        assert InfoThroughNodesHeading == TestData.InfoThroughNodesHeading

    def test_AreBlockchainSecure(self):
        driver = self.driver

        TransactionObj = Transaction(driver)
        TransactionObj.clickTransactionButton()
        time.sleep(5)
        TransactionObj.clickAreBlockchainSecureButton()
        AreBlockchainSecureHeading = TransactionObj.getAreBlockchainSecureHeading()
        assert AreBlockchainSecureHeading == TestData.BlockchainSecureHeading
